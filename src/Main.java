
public class Main {

    public static void main(String[] args) {
        Dog2[] dogs = insertDog();
        printDogsOnConsole(dogs);
        feed(dogs);
        System.out.println("After eating------------");
        printDogsOnConsole(dogs);
    }

    private static void feed(Dog2[] dogs) {
        for (int i = 0; i < dogs.length; i++) {
            // we give feed them with 500gr of food
            double weightBeforefeeding = dogs[i].getWeight();
            dogs[i].setWeight(weightBeforefeeding + 1.5);
        }

    }

    private static void printDogsOnConsole(Dog2[] dogs) {
        for (int i = 0; i < dogs.length; i++) {
            dogs[i].printToConsole();
        }

    }

    private static Dog2[] insertDog() {

        Dog2[] dogs = new Dog2[4];

        // list of Dogs
        String[] names = { "Coco", "Sultan", "Boby", "Drak" };
        String[] colours = { "brown", "black", "white", "blue" };
        double[] weight = { 1.5, 75, 3.5, 45.1 };

        for (int i = 0; i < dogs.length; i++) {
            Dog2 dog = new Dog2();
            dog.setName(names[i]);
            dog.setColour(colours[i]);
            dog.setWeight(weight[i]);
            dogs[i] = dog;

        }
        return dogs;
    }
}
